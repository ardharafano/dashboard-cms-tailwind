<!DOCTYPE html>
<html lang="en" class="light-style layout-menu-fixed">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - CMS</title>

    <meta name="description" content="Iklandisini.com">
    <meta name="keywords" content="Iklandisini.com">

    <link rel="Shortcut icon" href="assets/vendor/images/icon-suara.png" />

    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>


    <link rel="stylesheet" href="assets/vendor/css/core.css?<?= time() ?>" class="template-customizer-core-css" />
    <link rel="stylesheet" href="assets/vendor/css/theme-default.css?<?= time() ?>" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="assets/vendor/css/demo.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/vendor/css/perfect-scrollbar.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />

    <!-- tailwind -->
    <link rel="stylesheet" href="assets/vendor/src/tailwind.css?<?= time() ?>" />
    <!-- end tailwind -->

    <script src="assets/vendor/js/helpers.js?<?= time() ?>"></script>
    <script src="assets/vendor/js/config.js?<?= time() ?>"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

</head>

<body>

    <!-- Navbar -->

        <?php if(isset($_GET['profile'])) { 
            if($_GET['profile'] != 'login') { ?>
          <?php include ("include/blocks/navbar.php"); ?>
        <?php } } else { ?> 
        <?php }  ?>

    <!-- <?php // include('include/blocks/sidebar.php'); ?> -->
    <!-- End Navbar -->


    <?php
        if(isset($_GET['profile'])){ 
            $url = 'index.php?';
            include("include/profile/".$_GET['profile'].".php");
        }else{
            $url = 'index.php?';
            include("include/profile/login.php");
        }
    ?>



        <!-- <script src="https://code.jquery.com/jquery-3.6.1.slim.min.js" integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.6/dist/js/splide.min.js"></script> -->

        <script src="assets/vendor/js/jquery.js?<?= time() ?>"></script>
        <script src="assets/vendor/js/popper.js?<?= time() ?>"></script>
        <script src="assets/vendor/js/bootstrap.js?<?= time() ?>"></script>
        <script src="assets/vendor/js/perfect-scrollbar.js?<?= time() ?>"></script>
        <script src="assets/vendor/js/menu.js?<?= time() ?>"></script>
        <script src="assets/vendor/js/main.js?<?= time() ?>"></script>

        <script>
            $(document).ready(function() {
                jQuery('img').each(function() {
                    jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
                });
            });
        </script>

</body>

</html>