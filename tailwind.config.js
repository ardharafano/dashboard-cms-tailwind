/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./assets/vendor/src/**/*.{html,js}", "index.php", "./include/**/*"],
    theme: {
        extend: {},
        backgroundPosition: {
            zero: 'zero',
            'zero-0': '0rem'
        },
        screens: {
            'min-sm': '576px',
            // => @media (min-width: 576px) { ... }

            'min-md': '960px',
            // => @media (min-width: 960px) { ... }

            'min-lg': '1280px',
            // => @media (min-width: 1280px) { ... }

            'max-2xl': { 'max': '1535px' },
            // => @media (max-width: 1535px) { ... }

            'max-xl': { 'max': '1279px' },
            // => @media (max-width: 1279px) { ... }

            'max-lg': { 'max': '1023px' },
            // => @media (max-width: 1023px) { ... }

            'max-md': { 'max': '767px' },
            // => @media (max-width: 767px) { ... }

            'max-sm': { 'max': '639px' },
            // => @media (max-width: 639px) { ... }
        },
        // colors: {
        //     transparent: 'transparent',
        //     current: 'currentColor',
        //     'white': '#ffffff',
        //     'header': '#FC7615',
        //     'judul': '#440C0C',
        //     'button-login': '#78001B',
        //     'red': '#fc0303',
        //     'yellow': '#c98804',
        //     'button-edit': '#333458',
        //     'button-import': '#888888',
        //     'button-upload': '#804D00',
        //     'blue': '#428af5'
        // },
    },
    plugins: [],
}

// npx tailwindcss -i ./assets/vendor/src/input.css -o ./assets/vendor/src/tailwind.css --watch