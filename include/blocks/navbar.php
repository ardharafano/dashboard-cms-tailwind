<div class="layout-page sticky-top">

    <nav class="layout-navbar container navbar navbar-expand-xl navbar-detached items-center bg-navbar-theme"
        id="layout-navbar">
        <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
            <a class="nav-item nav-link px-0 xl:me-6" href="javascript:void(0)">
                <i class="bx bx-menu bx-sm"></i>
            </a>
        </div>

        <div class="navbar-nav-right flex items-center" id="navbar-collapse">
            <!-- Search -->

            <div class="flex flex-wrap list-reset pl-0 mb-0 items-center">
                <div class=" flex items-center">
                    <i class="bx bx-search fs-4 lh-0"></i>
                    <input type="text"
                        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-gray-800 border-gray-200 rounded border-0 shadow-none"
                        placeholder="Search..." aria-label="Search..." class="w-full" />
                </div>
            </div>
            <!-- /Search -->

            <ul class="navbar-nav flex-row items-center ml-auto">

                <!-- Place this tag where you want the button to render. -->
                <li class="nav-item mr-3">
                    <!-- <a href="#" class="nav-side">F&Q</a> -->
                    <a href="#">
                        <img src="assets/vendor/images/faq.svg" alt="img" width="28" height="auto" />
                    </a>
                </li>

                <li class="nav-item mr-3">
                    <a href="#">
                        <img src="assets/vendor/images/chat.svg" alt="img" width="28" height="auto" />
                    </a>
                </li>

                <!-- User -->
                <li class="nav-item navbar-dropdown dropdown-user dropdown">
                    <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                        <div class="avatar avatar-online">
                            <img src="assets/vendor/images/1.png" alt class="w-px-40 h-auto rounded-circle" />
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        <li>
                            <a class="dropdown-item" href="#">
                                <div class="flex">
                                    <div class="flex-shrink-0 me-3">
                                        <div class="avatar avatar-online">
                                            <img src="assets/vendor/images/1.png" alt class="w-10 h-auto rounded-3xl" />
                                        </div>
                                    </div>
                                    <div class="flex-grow">
                                        <span class="font-semibold block">John Doe</span>
                                        <small class="text-gray-700">Admin</small>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <div class="dropdown-divider"></div>
                        </li>
                        <li>
                            <a class="dropdown-item" href="?profile=home">
                                <i class="bx bx-user me-2"></i>
                                <span class="align-middle">My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="?profile=pages-account-settings-password">
                                <i class="bx bx-cog me-2"></i>
                                <span class="align-middle">Settings</span>
                            </a>
                        </li>
                        <li>
                            <div class="dropdown-divider"></div>
                        </li>
                        <li>
                            <a class="dropdown-item" href="index.php?page=login">
                                <i class="bx bx-power-off me-2"></i>
                                <span class="align-middle">Log Out</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--/ User -->
            </ul>
        </div>
    </nav>

    <!-- / Navbar -->
</div>