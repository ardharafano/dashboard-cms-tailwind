<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

    <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            <div class="app-brand demo">
                <a href="?profile=home" class="app-brand-link" target="_blank">
                    <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47" class="logo-side" />
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                    <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
            </div>

            <div class="menu-inner-shadow"></div>

            <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="?profile=home" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle"></i>
                        <div data-i18n="Analytics">Dashboard</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text">Konten</span>
                </li>

                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">Post</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=all-post" class="menu-link">
                                <!-- <i class='menu-icon bx bxs-message-alt-detail'></i> -->
                                <div data-i18n="Account">All Post</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=new-post" class="menu-link">
                                <!-- <i class='menu-icon bx bx-edit'></i> -->
                                <div data-i18n="Notifications">New</div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="menu-item">
                    <a href="?profile=galeri" class="menu-link">
                        <i class='menu-icon bx bx-photo-album'></i>
                        <div data-i18n="Analytics">Galeri</div>
                    </a>
                </li>

                <!-- Forms & Tables -->
                <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
                <li class="menu-item open active">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                        <div data-i18n="Account Settings">Account Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item active">
                            <a href="?profile=pages-account-settings-password" class="menu-link">
                                <div data-i18n="Notifications">Password</div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>

        <!-- Layout container -->
        <div class="layout-page">


            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Content -->

                <div class="container flex-grow">
                    <h4 class="font-bold my-3"><span class="font-light">Account Settings /</span> Password</h4>

                    <div class="row-auto">
                        <div class="columns-1">

                            <div class="card mb-4">

                                <hr class="my-0" />
                                <div class="card-body">
                                    <form id="formAccountSettings" method="POST" onsubmit="return false">
                                        <div class="row">

                                            <div class="card-account-password my-5">
                                                EDIT PASSWORD
                                            </div>

                                            <div class="mb-3 form-password-toggle">
                                                <div class="flex justify-between">
                                                    <label class="form-label" for="password">Password Saat Ini</label>
                                                </div>
                                                <div class="input-group input-group-merge">
                                                    <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                                                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                                </div>
                                            </div>
                                            <div class="mb-3 form-password-toggle">
                                                <div class="d-flex justify-content-between">
                                                    <label class="form-label" for="password">Password Baru</label>
                                                </div>
                                                <div class="input-group input-group-merge">
                                                    <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                                                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                                </div>
                                            </div>
                                            <div class="mb-3 form-password-toggle">
                                                <div class="d-flex justify-content-between">
                                                    <label class="form-label" for="password">Konfirmasi Password Baru</label>
                                                </div>
                                                <div class="input-group input-group-merge">
                                                    <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                                                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                                </div>
                                            </div>

                                            <div class="mt-2">
                                                <button type="submit" class="px-3 py-2 text-white rounded-md font-semibold bg-[#440C0C] mr-1">Simpan</button>
                                                <button type="reset" class="border px-3 py-2 rounded-md font-semibold">Batal</button>
                                            </div>

                                    </form>
                                    </div>
                                    <!-- /Account -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- / Content -->

                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->