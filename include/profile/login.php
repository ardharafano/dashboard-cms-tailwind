<div
    class="flex justify-center items-center bg-[url('../images/login.jpg')] bg-cover bg-no-repeat bg-center w-full min-h-screen max-lg:bg-zero-0 ">
    <div class="container max-w-lg sm:px-4">
        <div class="authentication-wrapper authentication-basic">
            <div class="authentication-inner">
                <!-- Register -->
                <div
                    class="relative flex flex-col min-w-0 rounded break-words border bg-white border-1 border-gray-300">
                    <div class="flex-auto p-6">
                        <!-- Logo -->
                        <div class="app-brand justify-center">
                            <a href="?profile=login" class="app-brand-link gap-2">
                                <span class="app-brand-logo demo">
                                    <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137"
                                        height="47" />
                                </span>
                            </a>
                        </div>
                        <!-- /Logo -->
                        <h4 class="text-xl text-center mt-3 text-[#FC7615] font-medium">Hello,</h4>
                        <h2 class="text-3xl mb-3 font-bold text-[#FC7615] text-center">Welcome!</h2>

                        <form id="formAuthentication" class="mb-3" action="?profile=home" method="POST">

                            <div class="mb-3 text-center">
                                <label for="partner" class="form-label text-[#440C0C] font-bold">Partner Name</label>
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Pilih Partner</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>

                            <div class="mb-3 text-center">
                                <label for="email" class="form-label text-[#440C0C] font-bold">Email</label>
                                <input type="text"
                                    class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-gray-800 border border-gray-200 rounded"
                                    id="email" name="email-username" placeholder="Enter your email" autofocus />
                            </div>
                            <div class="mb-3 form-password-toggle">
                                <div class="text-center">
                                    <label class="form-label text-[#440C0C] font-bold" for="password">Password</label>
                                </div>
                                <div class="relative flex items-stretch w-full input-group-merge">
                                    <input type="password" id="password"
                                        class="block appearance-none w-full py-1 px-2 mb-1 text-base leading-normal bg-white text-gray-800 border border-gray-200 rounded"
                                        name="password"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="password" />
                                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="remember-me" />
                                    <label class="form-check-label" for="remember-me"> Remember Me </label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button
                                    class="bg-[#78001B] text-white align-middle text-center select-none border font-medium whitespace-no-wrap rounded-2xl py-2 px-3 leading-normal no-underline grid w-full"
                                    type="submit">LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /Register -->
            </div>
        </div>
    </div>
</div>

<!-- / Content -->