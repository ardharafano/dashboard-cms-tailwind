<head>
    <script src="https://cdn.ckeditor.com/4.20.1/standard/ckeditor.js"></script>
</head>

<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            <div class="app-brand demo">
                <a href="?profile=home" class="app-brand-link" target="_blank">
                    <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47" class="logo-side" />
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                    <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
            </div>

            <div class="menu-inner-shadow"></div>

            <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="?profile=home" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle"></i>
                        <div data-i18n="Analytics">Dashboard</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text">Konten</span>
                </li>

                <li class="menu-item open active">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">Post</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=all-post" class="menu-link">
                                <!-- <i class='menu-icon bx bxs-message-alt-detail'></i> -->
                                <div data-i18n="Account">All Post</div>
                            </a>
                        </li>
                        <li class="menu-item active">
                            <a href="?profile=new-post" class="menu-link">
                                <!-- <i class='menu-icon bx bx-edit'></i> -->
                                <div data-i18n="Notifications">New</div>
                            </a>
                        </li>
                    </ul>
                </li>



                <li class="menu-item">
                    <a href="?profile=galeri" class="menu-link">
                        <i class='menu-icon bx bx-photo-album'></i>
                        <div data-i18n="Analytics">Galeri</div>
                    </a>
                </li>

                <!-- Forms & Tables -->
                <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                        <div data-i18n="Account Settings">Account Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-password" class="menu-link">
                                <div data-i18n="Notifications">Password</div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">

            <div class="card-bg my-3">
                <!-- <h5 class="card-account">Profile Details</h5> -->

                <header>
                    <div class="container">
                        <div class="flex flex-wrap">
                            <h1 class="text-2xl mb-1 mx-4 font-bold mt-5 text-white">Create New Post</h1>
                            <!-- <p class="ps-5 mb-5 text-white">Experience a simple yet powerful way to build Dasboards with Iklandisini</p> -->
                        </div>
                    </div>
                </header>

            </div>


            <!-- content wrapper -->
            <div class="content-wrapper">
                <!-- Dashboard  -->
                <div class="container flex-grow">
                    <div class="card mb-4" style="position:relative;top:-150px">

                        <div id="new-post">
                            <div class="container">

                                <div class="flex justify-between content-center items-center my-4">
                                    <div>
                                        <h1 class="mb-3 font-bold mt-5 pl-5 text-2xl" style="color:#440C0C">NEW</h1>
                                    </div>

                                    <div class="flex justify-center">
                                        <a href="#" class="menu-link button-backup">
                                            <i class='menu-icon bx bx-data'></i>
                                            <div> Backup</div>
                                        </a>
                                        <a href="#" class="menu-link button-reset">
                                            <i class='menu-icon bx bx-reset'></i>
                                            <div>Reset</div>
                                        </a>
                                    </div>
                                </div>

                                <div class="flex min-lg:flex-nowrap max-lg:flex-wrap ">
                                    <div class="min-lg:w-2/3 w-full pr-4 pl-4">
                                        <div class="my-3">
                                            <input type="text" class="form-control-title" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ADD TITLE">
                                            <input type="text" class="form-control-summary" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ADD SUMMARY" style="color:#000">
                                        </div>
                                        <textarea name="editor1"></textarea>
                                        <script>
                                                CKEDITOR.replace( 'editor1' );
                                        </script>
                                    </div>

                                    <div class="min-lg:w-1/3 w-full pr-4 pl-4 my-3 bg-side">
                                        <div class="my-3">Post Type:</div>
                                        <ul class="nav nav-pills justify-center mb-3" id="pills-tab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="pills-article-tab" data-bs-toggle="pill" data-bs-target="#pills-article" type="button" role="tab" aria-controls="pills-article" aria-selected="true">Article</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video</button>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="pills-article" role="tabpanel" aria-labelledby="pills-article-tab">

                                                <div>
                                                    <div>Backup Version(s)</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Backup</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Featured Image</div>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="bg-[#78001B] text-white align-middle text-center select-none border font-medium whitespace-no-wrap rounded-2xl py-2 px-3 leading-normal no-underline grid w-full" data-bs-toggle="modal" data-bs-target="#exampleModal5">
                                                        Add Image
                                                    </button>

                                                    <!-- Modal -->

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel5" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="exampleModalLabel5">Select Image</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">

                                                                    <div class="flex flex-wrap">
                                                                        <div class="min-lg:w-3/4 pr-4 pl-4 min-lg:mb-0 mb-3">
                                                                            <div>All Image <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
                                                                            </div>
                                                                        </div>

                                                                        <div class="min-lg:w-3/4 pr-4 pl-4 min-lg:mb-0 mb-3">
                                                                            <div class="input-group">
                                                                                <div class="form-outline">
                                                                                    <input type="search" id="form1" class="form-control" placeholder="Search" />
                                                                                    <!-- <label class="form-label" for="form1">Search</label> -->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="min-lg:w-1/4 pr-4 pl-4 my-2">
                                                                        <a href="#">
                                                                            <img src="https://demo.suara.com/arkadia.suara.sit/iklan-native/img/list-headline.png" alt="img" class="featured">
                                                                            <div class="title">Ilustrasi Logo Seria A Italia</div>
                                                                            <div class="date">04 Jan 2023 22:15</div>
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary">Select</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="my-4">
                                                    <div>Artikel Dewasa:</div>
                                                    <div style="border:1px solid red;padding:5px;border-radius:5px;margin-top:5px">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate1">
                                                            <label class="form-check-label" for="flexCheckIndeterminate1">
                                                            Artikel mengandung konten dewasa 18+
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="my-4">
                                                    <div>Flags:</div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate2">
                                                        <label class="form-check-label" for="flexCheckIndeterminate2">
                                                            Headline
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate3">
                                                        <label class="form-check-label" for="flexCheckIndeterminate3">
                                                            Pilihan
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="my-4">
                                                    <div>Category:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Category</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Author:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Author</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Topic:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Topic</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000">
                                                        <span class="input-group-text" id="inputGroup-sizing-default">Tags</span>
                                                    </div>
                                                    <small>Press "Enter" to confirm</small>
                                                </div>

                                                <div>Terkait:</div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000" disabled>
                                                    <button type="button" class="input-group-text bg-white" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                                                        ADD
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="exampleModalLabel1">Pilih Postingan Terkait</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <small>Silakan pilih setidaknya 3 posts</small>
                                                                    <div class="input-group rounded">
                                                                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                                                        <span class="input-group-text border-0" id="search-addon">
                                                                            Search
                                                                        </span>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div>Baca Juga:</div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000" disabled>

                                                    <button type="button" class="input-group-text bg-white" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                                        ADD
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="exampleModalLabel2">Pilih Postingan Lainnya</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <small>Silakan pilih setidaknya 3 posts</small>
                                                                    <div class="input-group rounded">
                                                                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                                                        <span class="input-group-text border-0" id="search-addon">
                                                                            Search
                                                                        </span>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="my-4">
                                                    <div>Publish:</div>
                                                    <input type="date" id="date" name="date">
                                                </div>

                                                <div class="my-3">
                                                    <a href="#">
                                                        <div class="button-draft">Draft & Preview</div>
                                                    </a>
                                                </div>

                                                <div class="my-3">
                                                    <a href="#">
                                                        <div class="button-cancel">Cancel</div>
                                                    </a>
                                                </div>

                                            </div>

                                            <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">

                                                <div>
                                                    <div>Backup Version(s)</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Backup</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Featured Image</div>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop2">
                                                        Add Image
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="staticBackdropLabel">Select Image</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">

                                                                    <div class="flex flex-wrap">
                                                                        <div class="min-lg:w-3/4 pr-4 pl-4">
                                                                            <div>All Image <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
                                                                            </div>
                                                                        </div>

                                                                        <div class="min-lg:w-1/4 pr-4 pl-4">
                                                                            <div class="input-group">
                                                                                <div class="form-outline">
                                                                                    <input type="search" id="form1" class="form-control" placeholder="Search" />
                                                                                    <!-- <label class="form-label" for="form1">Search</label> -->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="flex flex-wrap">
                                                                        <div class="lg:w-1/4 pr-4 pl-4 my-2">
                                                                            <a href="#">
                                                                                <img src="https://demo.suara.com/arkadia.suara.sit/iklan-native/img/list-headline.png" alt="img" class="featured">
                                                                                <div class="title">Ilustrasi Logo Seria A Italia</div>
                                                                                <div class="date">04 Jan 2023 22:15</div>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary">Select</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div>Video URL (Youtube):</div>
                                                <div class="input-group">
                                                    <input type="url" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000" placeholder="https://">
                                                </div>

                                                <div class="my-4">
                                                    <div>Artikel Dewasa:</div>
                                                    <div style="border:1px solid red;padding:5px;border-radius:5px;margin-top:5px">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate1">
                                                            <label class="form-check-label" for="flexCheckIndeterminate1">
                                                            Artikel mengandung konten dewasa 18+
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="my-4">
                                                    <div>Flags:</div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate2">
                                                        <label class="form-check-label" for="flexCheckIndeterminate2">
                                                            Headline
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate3">
                                                        <label class="form-check-label" for="flexCheckIndeterminate3">
                                                            Pilihan
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="my-4">
                                                    <div>Category:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Category</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Author:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Author</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div>Topic:</div>
                                                    <select class="form-select" aria-label="Default select example">
                                                    <option selected>Select Topic</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                                </div>

                                                <div class="my-4">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000">
                                                        <span class="input-group-text" id="inputGroup-sizing-default">Tags</span>
                                                    </div>
                                                    <small>Press "Enter" to confirm</small>
                                                </div>

                                                <div>Terkait:</div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000" disabled>
                                                    <button type="button" class="input-group-text" data-bs-toggle="modal" data-bs-target="#exampleModal3">
                                                        ADD
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel3" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="exampleModalLabel1">Pilih Postingan Terkait</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <small>Silakan pilih setidaknya 3 posts</small>
                                                                    <div class="input-group rounded">
                                                                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                                                        <span class="input-group-text border-0" id="search-addon">
                                                                            Search
                                                                        </span>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div>Baca Juga:</div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="color:#000" disabled>

                                                    <button type="button" class="input-group-text" data-bs-toggle="modal" data-bs-target="#exampleModal4">
                                                        ADD
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel4" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                            <div class="modal-content modal-xl">

                                                                <div class="modal-header modal-xl">
                                                                    <h5 class="modal-title" id="exampleModalLabel2">Pilih Postingan Lainnya</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <small>Silakan pilih setidaknya 3 posts</small>
                                                                    <div class="input-group rounded">
                                                                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                                                        <span class="input-group-text border-0" id="search-addon">
                                                                            Search
                                                                        </span>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="my-5">
                                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="90" height="90" class="terkait" />
                                                                        <div class="flex justify-between">
                                                                            <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022, Media Vietnam Sindir Tragedi Kanjuruhan</span>

                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                                                            </div>
                                                                        </div>

                                                                        <div class="flex content-center items-center">
                                                                            <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                            <div class="tag">Yoursay</div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="my-4">
                                                    <div>Publish:</div>
                                                    <input type="date" id="date" name="date">
                                                </div>

                                                <div class="my-3">
                                                    <a href="#">
                                                        <div class="button-draft">Draft & Preview</div>
                                                    </a>
                                                </div>

                                                <div class="my-3">
                                                    <a href="#">
                                                        <div class="button-cancel">Cancel</div>
                                                    </a>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="content-backdrop fade"></div>
                        </div>
                        <!-- Content wrapper -->
                    </div>
                    <!-- / Layout page -->
                </div>
                <!-- end content wrapper -->


                <!-- <div class="content-backdrop fade"></div> -->
            </div>
            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->