<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
            <div class="app-brand demo">
                <a href="?profile=home" class="app-brand-link" target="_blank">
                    <img src="assets/vendor/images/arkadia.svg" alt="iklandisini.com" width="137" height="47"
                        class="logo-side" />
                </a>

                <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                    <i class="bx bx-chevron-left bx-sm align-middle"></i>
                </a>
            </div>

            <div class="menu-inner-shadow"></div>

            <ul class="menu-inner py-1">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="?profile=home" class="menu-link">
                        <i class="menu-icon tf-icons bx bx-home-circle"></i>
                        <div data-i18n="Analytics">Dashboard</div>
                    </a>
                </li>


                <li class="menu-header small text-uppercase">
                    <span class="menu-header-text">Konten</span>
                </li>

                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-detail"></i>
                        <div data-i18n="Account Settings">Post</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=all-post" class="menu-link">
                                <!-- <i class='menu-icon bx bxs-message-alt-detail'></i> -->
                                <div data-i18n="Account">All Post</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="?profile=new-post" class="menu-link">
                                <!-- <i class='menu-icon bx bx-edit'></i> -->
                                <div data-i18n="Notifications">New</div>
                            </a>
                        </li>
                    </ul>
                </li>



                <li class="menu-item active">
                    <a href="?profile=pages-topup" class="menu-link">
                        <i class='menu-icon bx bx-photo-album'></i>
                        <div data-i18n="Analytics">Galeri</div>
                    </a>
                </li>

                <!-- Forms & Tables -->
                <li class="menu-header small text-uppercase"><span class="menu-header-text">Setting</span></li>
                <li class="menu-item">
                    <a href="javascript:void(0);" class="menu-link menu-toggle">
                        <i class="menu-icon tf-icons bx bx-lock-open-alt"></i>
                        <div data-i18n="Account Settings">Account Settings</div>
                    </a>
                    <ul class="menu-sub">
                        <li class="menu-item">
                            <a href="?profile=pages-account-settings-password" class="menu-link">
                                <div data-i18n="Notifications">Password</div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">

            <div class="card-bg my-3">
                <!-- <h5 class="card-account">Profile Details</h5> -->

                <header>
                    <div class="container">
                        <div class="row-auto">
                            <h1 class="text-2xl mb-1 mx-4 font-bold mt-5 text-white">All Galleries</h1>
                        </div>
                    </div>
                </header>

            </div>


            <!-- content wrapper -->
            <div class="content-wrapper">
                <!-- Dashboard  -->
                <div class="container flex-grow">
                    <div class="card mb-4" style="position:relative;top:-150px">
                        <hr class="my-0" />

                        <div id="galeri">
                            <div class="container">
                                <div class="min-lg:flex justify-between content-center items-center my-4">
                                    <div>
                                        <h1 class="h3 fw-bold mt-3 ps-1" style="color:#440C0C">GALERI</h1>
                                    </div>

                                    <div class="flex justify-center">
                                        <a href="#"
                                            class="menu-link bg-[#888888] rounded-xl mr-3 font-bold p-2 text-white"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal1">
                                            <i class='menu-icon bx bx-import'></i>
                                            <div class="">Import Image</div>
                                        </a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal1" tabindex="-1"
                                            aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                            <div
                                                class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
                                                <div class="modal-content modal-xl">

                                                    <div class="modal-header modal-xl">
                                                        <h5 class="modal-title" id="exampleModalLabel1">Bank Foto
                                                            Suara.com</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="input-group rounded">
                                                            <input type="search" class="form-control rounded"
                                                                placeholder="Search" aria-label="Search"
                                                                aria-describedby="search-addon" />
                                                            <span class="input-group-text border-0" id="search-addon">
                                                                Search
                                                            </span>
                                                        </div>

                                                        <div class="my-5">
                                                            <img src="assets/vendor/images/post.png"
                                                                alt="iklandisini.com" width="90" height="90"
                                                                class="terkait" />
                                                            <div class="flex justify-between">
                                                                <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022,
                                                                    Media Vietnam Sindir Tragedi Kanjuruhan</span>
                                                            </div>

                                                            <div class="flex content-center items-center">
                                                                <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                <div class="tag">Yoursay</div>
                                                            </div>
                                                        </div>

                                                        <div class="my-5">
                                                            <img src="assets/vendor/images/post.png"
                                                                alt="iklandisini.com" width="90" height="90"
                                                                class="terkait" />
                                                            <div class="flex justify-between">
                                                                <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022,
                                                                    Media Vietnam Sindir Tragedi Kanjuruhan</span>
                                                            </div>

                                                            <div class="flex content-center items-center">
                                                                <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                <div class="tag">Yoursay</div>
                                                            </div>
                                                        </div>

                                                        <div class="my-5">
                                                            <img src="assets/vendor/images/post.png"
                                                                alt="iklandisini.com" width="90" height="90"
                                                                class="terkait" />
                                                            <div class="flex justify-between">
                                                                <span>FIFA Turun Tangan Jelang Semifinal Piala AFF 2022,
                                                                    Media Vietnam Sindir Tragedi Kanjuruhan</span>
                                                            </div>

                                                            <div class="flex content-center items-center">
                                                                <div class="date">19 Jan 2023 | 15:10 WIB</div>
                                                                <div class="tag">Yoursay</div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary">Import</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <label for="file-img"
                                                class="bg-[#804D00] rounded-xl mr-3 font-bold p-2 text-white"><i
                                                    class='menu-icon bx bx-upload'></i>Upload</label>
                                            <input type="file" id="file-img" class="button-upload menu-link" />
                                        </div>

                                    </div>
                                </div>

                                <div class="input-group">
                                    <input type="search" class="form-control rounded" placeholder="Search"
                                        aria-label="Search" aria-describedby="search-addon" />
                                    <button type="button" class="btn btn-outline-primary">search</button>
                                </div>


                                <hr class="my-0">
                                <div class="flex flex-wrap my-4">
                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close pb-3"
                                                            data-bs-dismiss="modal" aria-label="Close">&#10005;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="bg-white border p-2 rounded-3xl"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button"
                                                            class="bg-[#428af5] text-white p-2 rounded-3xl"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="bg-red text-white p-2 rounded-3xl"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="bg-[#c98804] text-white p-2 rounded-3xl">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="min-lg:w-1/4 pr-4 pl-4">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            <img src="assets/vendor/images/post.png" alt="iklandisini.com" width="243"
                                                height="243" class="home" />
                                        </a>
                                        <div class="title">Ilustrasi Logo Seria A Italia</div>
                                        <div class="date">04 Jan 2023 22:15</div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1"
                                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="assets/vendor/images/post.png" alt="iklandisini.com"
                                                            width="90" height="90" class="popup" />
                                                        <div class="input-group my-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Title</span>
                                                            <input type="text" class="form-control"
                                                                value="Ilustrasi Logo Seria A Italia"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text"
                                                                id="inputGroup-sizing-default">Source</span>
                                                            <input type="text" class="form-control"
                                                                value="[ANTARA/Gilang Galiartha]"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-default">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary rounded-pill"
                                                            data-bs-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-info rounded-pill"
                                                            data-bs-dismiss="modal">Update</button>
                                                        <button type="button" class="btn btn-danger rounded-pill"
                                                            data-bs-dismiss="modal">Disable</button>
                                                        <button type="button"
                                                            class="btn btn-warning rounded-pill">Regenerate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>

                                <div class="flex justify-center items-center gap-4 mb-2 font-semibold">
                                    <a href="#">&laquo;</a>
                                    <a href="#">1</a>
                                    <a href="#"
                                        class="bg-[#78001B] rounded-full text-center pt-2 text-white w-10 h-10">2</a>
                                    <a href="#">3</a>
                                    <a href="#">4</a>
                                    <a href="#">5</a>
                                    <a href="#">&raquo;</a>
                                </div>

                            </div>



                        </div>


                        <div class="content-backdrop fade"></div>
                    </div>
                    <!-- Content wrapper -->
                </div>
                <!-- / Layout page -->
            </div>
            <!-- end content wrapper -->


            <!-- <div class="content-backdrop fade"></div> -->
        </div>
        <!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->